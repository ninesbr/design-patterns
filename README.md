### Padrões de projetos

Em Engenharia de Software, um padrão de desenho ou padrão de projeto é uma solução geral para um problema que ocorre com frequência dentro de um determinado contexto no projeto de software.

# Padrões de Criação


Os Padrões de Criação estão associados diretamente com os mecanismos que controlam a criação de objetos. Em alguns projetos, a tarefa de criar um objeto pode ser algo problemático e possuir algumas particularidades, levando à um nível alto e desnecessário de complexidade. Com isso, o objetivo dos Padrões de Criação é evitar problemas e proporcionar maior controle na tarefa de criação de objetos, separando o processo de criação, conclusão e representação de um objeto.

- Os padrões para implementação de controle de criação de objetos são:
    - [Factory Method](https://gitlab.com/pocs8/design-patterns/-/tree/master/factoryMethod): É um dos padrões de projeto mais conhecidos e tem o objetivo de ocultar detalhes sobre a criação de objetos, fornecendo uma interface para que isso aconteça. A classe que implementa a interface decidirá qual objeto será criado, dentre muitos possíveis.
    - [Singleton](https://gitlab.com/pocs8/design-patterns/-/tree/master/singleton): Ao projetar um sistema, pode-se desejar que uma classe tenha no máximo uma instância. Esse padrão visa garantir essa característica provendo um ponto de acesso global à classe, ou seja, apenas um objeto de uma determinada classe poderá existir, independentemente do número de requisições que sejam feitas para criação de um novo objeto.
    - [Builder](https://gitlab.com/pocs8/design-patterns/-/tree/master/builder): Separar a construção de um objeto complexo de sua representação de modo que o mesmo processo de construção possa criar diferentes representaçõe
    - [Abstract Factory](https://gitlab.com/pocs8/design-patterns/-/tree/master/abstractFactory): Este padrão permite a criação de famílias de objetos relacionados ou dependentes por meio de uma única interface e sem que a classe concreta seja especificada. Uma fábrica é a localização de uma classe concreta no código em que objetos são construídos.
    - [Prototype](https://gitlab.com/pocs8/design-patterns/-/tree/master/prototype): Especificar tipos de objetos a serem criados usando uma instância protótipo e criar novos objetos pela cópia desse protótipo.
    
# Tecnologias
- Java 1.8
- Junit 5

# Execução

A execução das aplicações são feitas através do de um comando Maven que envoca executa do teste unitário

- Scripts
    - ```mvn clean install test ```