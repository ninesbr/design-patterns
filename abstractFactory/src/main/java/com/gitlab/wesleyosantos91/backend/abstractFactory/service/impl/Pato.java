package com.gitlab.wesleyosantos91.backend.abstractFactory.service.impl;

import com.gitlab.wesleyosantos91.backend.abstractFactory.service.Animal;

public class Pato implements Animal {

    @Override
    public String getAnimal() {
        return "Pato";
    }

    @Override
    public String getSom() {
        return "Quá Quá";
    }
}
