package com.gitlab.wesleyosantos91.backend.abstractFactory.service;

public interface Animal {

    String getAnimal();
    String getSom();
}
