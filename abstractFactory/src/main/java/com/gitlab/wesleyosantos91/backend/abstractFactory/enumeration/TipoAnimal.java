package com.gitlab.wesleyosantos91.backend.abstractFactory.enumeration;

public enum TipoAnimal {

    CACHORRO,
    PATO
}
