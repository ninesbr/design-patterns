package com.gitlab.wesleyosantos91.backend.factoryMethod.service.impl;

import com.gitlab.wesleyosantos91.backend.factoryMethod.enumeration.Lados;
import com.gitlab.wesleyosantos91.backend.factoryMethod.service.Poligono;

public class Petagono implements Poligono {

    @Override
    public Integer getNumeroDeLados() {
        return Lados.CINCO.getValor();
    }
}
