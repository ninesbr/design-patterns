package com.gitlab.wesleyosantos91.backend.factoryMethod.enumeration;

public enum Lados {
    TRES(3),
    QUATRO(4),
    CINCO(5);

    private Integer valor;

    Lados(Integer valor) {
        this.valor = valor;
    }

    public Integer getValor() {
        return valor;
    }

    public static Lados getLados(Integer valor) {

        for (Lados lados : Lados.values()) {
            if (valor == lados.getValor()) {
                return lados;
            }
        }
        throw new IllegalArgumentException("Poligno não encontrado");
    }

}
