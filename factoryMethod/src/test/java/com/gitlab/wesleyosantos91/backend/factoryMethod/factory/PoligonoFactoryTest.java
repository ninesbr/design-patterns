package com.gitlab.wesleyosantos91.backend.factoryMethod.factory;

import com.gitlab.wesleyosantos91.backend.factoryMethod.enumeration.Lados;
import com.gitlab.wesleyosantos91.backend.factoryMethod.service.Poligono;
import com.gitlab.wesleyosantos91.backend.factoryMethod.service.impl.Petagono;
import com.gitlab.wesleyosantos91.backend.factoryMethod.service.impl.Quadrado;
import com.gitlab.wesleyosantos91.backend.factoryMethod.service.impl.Triangulo;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PoligonoFactoryTest {

    @Test
    @DisplayName("Deve retornar uma instancia de Quadrado")
    void getQuadrado() {
        assertTrue(PoligonoFactory.getPoligono(Lados.getLados(4)) instanceof Quadrado);
    }

    @Test
    @DisplayName("Deve retornar uma instancia de Triangulo")
    void getTriangulo() {

        Poligono poligono = PoligonoFactory.getPoligono(Lados.getLados(3));
        assertAll(
                () -> assertTrue(poligono instanceof Triangulo),
                () -> assertSame(poligono.getNumeroDeLados(), Lados.TRES.getValor())
        );
    }

    @Test
    @DisplayName("Deve retornar uma instancia de Petagono")
    void getPetagono() {

        Poligono poligono = PoligonoFactory.getPoligono(Lados.getLados(5));
        assertAll(
                () -> assertTrue(poligono instanceof Petagono),
                () -> assertSame(poligono.getNumeroDeLados(), Lados.CINCO.getValor())
        );
    }

    @Test
    @DisplayName("Deve retornar Falso devido ser um triangulo")
    void validarRetorno() {

        Poligono poligono = PoligonoFactory.getPoligono(Lados.getLados(4));
        assertAll(
                () -> assertTrue(poligono instanceof Quadrado),
                () -> assertSame(poligono.getNumeroDeLados(), Lados.QUATRO.getValor())
        );
    }

    @Test
    @DisplayName("Deve retornar uma exception")
    void getException() {
        assertThrows(IllegalArgumentException.class, () -> {
            Lados.getLados(50);
        });
    }
}