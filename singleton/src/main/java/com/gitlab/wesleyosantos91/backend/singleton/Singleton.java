package com.gitlab.wesleyosantos91.backend.singleton;

import java.util.Objects;

public class Singleton {

    private static Singleton instancia = null;

    private Singleton() {
    }

    public static Singleton getInstancia() {

        if (Objects.isNull(instancia)) {
            instancia = new Singleton();
        }
        return instancia;
    }
}
