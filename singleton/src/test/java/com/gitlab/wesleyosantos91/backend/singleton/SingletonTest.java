package com.gitlab.wesleyosantos91.backend.singleton;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SingletonTest {

    @Test
    @DisplayName("Deve validar se o método getInstancia está retornando a mesma instancia de obj")
    void getInstancia() {

        Singleton primeiraInstancia = Singleton.getInstancia();
        Singleton segundaInstancia = Singleton.getInstancia();

        assertAll(
                () -> assertEquals(primeiraInstancia, segundaInstancia),
                () -> assertSame(primeiraInstancia, segundaInstancia),
                () -> assertNotNull(Singleton.getInstancia())
        );
    }
}