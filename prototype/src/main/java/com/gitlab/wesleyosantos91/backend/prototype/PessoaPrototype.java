package com.gitlab.wesleyosantos91.backend.prototype;

import java.time.LocalDate;

public class PessoaPrototype extends Prototype {

    private String nome;
    private String sobreNome;
    private LocalDate dataNascimento;

    @Override
    public Prototype copy() {
        PessoaPrototype pessoaPrototype = new PessoaPrototype();

        pessoaPrototype.setNome(this.nome);
        pessoaPrototype.setSobreNome(this.sobreNome);
        pessoaPrototype.setDataNascimento(this.dataNascimento);
        return pessoaPrototype;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobreNome() {
        return sobreNome;
    }

    public void setSobreNome(String sobreNome) {
        this.sobreNome = sobreNome;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
