package com.gitlab.wesleyosantos91.backend.prototype;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrototypeTest {

    @Test
    void devecriarUmClone() {

        String wesley = "Wesley";
        String wellington = "Wellington";

        PessoaPrototype p = new PessoaPrototype();
        p.setNome(wesley);

        PessoaPrototype p1 = (PessoaPrototype) p.copy();
        p1.setNome(wellington);

       assertAll( () -> assertEquals(wesley, p.getNome()),
               () -> assertEquals(wellington, p1.getNome()));

    }
}