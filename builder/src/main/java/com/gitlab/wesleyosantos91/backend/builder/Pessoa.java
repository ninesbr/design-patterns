package com.gitlab.wesleyosantos91.backend.builder;

import java.time.LocalDate;

public class Pessoa {

    private String nome;
    private String sobreNome;
    private LocalDate dataNascimento;

    public Pessoa() {
    }

    private Pessoa(String nome, String sobreNome, LocalDate dataNascimento) {
        this.nome = nome;
        this.sobreNome = sobreNome;
        this.dataNascimento = dataNascimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobreNome() {
        return sobreNome;
    }

    public void setSobreNome(String sobreNome) {
        this.sobreNome = sobreNome;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public static class PessoaBuilder {

        private String nome;
        private String sobreNome;
        private LocalDate dataNascimento;

        public PessoaBuilder() {
        }

        public PessoaBuilder nome(String nome) {
            this.nome = nome;
            return this;
        }

        public PessoaBuilder sobreNome(String sobreNome) {
            this.sobreNome = sobreNome;
            return this;
        }

        public PessoaBuilder dataNascimento(LocalDate dataNascimento) {
            this.dataNascimento = dataNascimento;
            return this;
        }

        public Pessoa build() {
            return new Pessoa(nome, sobreNome, dataNascimento);
        }

    }
}
